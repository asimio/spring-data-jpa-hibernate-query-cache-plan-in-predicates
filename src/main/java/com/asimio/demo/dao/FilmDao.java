package com.asimio.demo.dao;

import java.util.List;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.EntityGraph.EntityGraphType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;

import com.asimio.demo.dao.filter.NamedQueryParametersSpecification;
import com.asimio.demo.dao.support.AsimioJpaSpecificationExecutor;
import com.asimio.demo.domain.Film;

/**
 * Extending {@link AsimioJpaSpecificationExecutor} allows execution of {@see NamedQueryParametersSpecification} 
 * {@link Specification}s based on the JPA criteria API FIXME Hibernate's in_clause_parameter_padding configuration property
 *
 * @author Orlando L Otero
 */
@Repository
public interface FilmDao extends JpaRepository<Film, Integer>, AsimioJpaSpecificationExecutor<Film> {

    @EntityGraph(
            type = EntityGraphType.FETCH,
            attributePaths = {
                    "language", 
                    "filmActors", "filmActors.actor",
                    "filmCategories", "filmCategories.category"
            }
    )
    List<Film> findAll(@Nullable NamedQueryParametersSpecification<Film> specification);

    @EntityGraph(
            type = EntityGraphType.FETCH,
            attributePaths = {
                    "language", 
                    "filmActors", "filmActors.actor",
                    "filmCategories", "filmCategories.category"
            }
    )
    List<Film> findAll(@Nullable Specification<Film> spec);
}
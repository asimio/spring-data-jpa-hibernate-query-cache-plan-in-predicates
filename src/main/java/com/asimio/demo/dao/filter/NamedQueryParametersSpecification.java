package com.asimio.demo.dao.filter;

import java.util.Map;

import org.springframework.data.jpa.domain.Specification;

public interface NamedQueryParametersSpecification<E> {

    Map<String, Object> getNamedQueryParameters();

    Specification<E> getSpecification();
}
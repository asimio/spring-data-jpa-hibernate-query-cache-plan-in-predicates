package com.asimio.demo.dao.filter.film;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.Set;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.ParameterExpression;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.CollectionUtils;

import com.asimio.demo.dao.filter.AbstractNamedQueryParametersSpecification;
import com.asimio.demo.domain.Category;
import com.asimio.demo.domain.Category_;
import com.asimio.demo.domain.Film;
import com.asimio.demo.domain.FilmCategory;
import com.asimio.demo.domain.FilmCategory_;
import com.asimio.demo.domain.Film_;
import com.asimio.demo.service.FilmSearchCriteria;

public final class FilmSpecificationsNamedParameters extends AbstractNamedQueryParametersSpecification<Film> {

    private static final String QUERY_PARAM_CATEGORIES = "filmCategories";

    private FilmSpecificationsNamedParameters() {
    }

    public static FilmSpecificationsNamedParameters createFilmSpecifications(FilmSearchCriteria searchCriteria) {
        FilmSpecificationsNamedParameters result = new FilmSpecificationsNamedParameters();
        result.specification = result.rentalRateBetween(searchCriteria.getMinRentalRate(), searchCriteria.getMaxRentalRate())
                .and(result.releaseYearEqualTo(searchCriteria.getReleaseYear()))
                .and(result.categoryIn(searchCriteria.getCategories()));
        return result;
    }

    private Specification<Film> rentalRateBetween(Optional<BigDecimal> minRate, Optional<BigDecimal> maxRate) {
        return (root, query, builder) -> {
            return minRate.map(min -> {
                return maxRate.map(max -> builder.between(root.get(Film_.rentalRate), min, max)
                ).orElse(null);
            }).orElse(null);
        };
    }

    private Specification<Film> releaseYearEqualTo(Optional<Long> releaseYear) {
        return (root, query, builder) -> {
            return releaseYear.map(relYear -> builder.equal(root.get(Film_.releaseYear), String.valueOf(relYear))
            ).orElse(null);
        };
    }

    @SuppressWarnings("rawtypes")
    private Specification<Film> categoryIn(Set<String> categories) {
        if (CollectionUtils.isEmpty(categories)) {
            return null;
        }
        return (root, query, builder) -> {
            Join<Film, FilmCategory> filmCategoryJoin = root.join(Film_.filmCategories);
            Join<FilmCategory, Category> categoryJoin = filmCategoryJoin.join(FilmCategory_.category);
            ParameterExpression<Set> queryParamCategories = this.createQueryParameter(
                    Set.class,
                    QUERY_PARAM_CATEGORIES,
                    categories,
                    builder
            );
            return categoryJoin.get(Category_.name).in(queryParamCategories);
        };
    }
}
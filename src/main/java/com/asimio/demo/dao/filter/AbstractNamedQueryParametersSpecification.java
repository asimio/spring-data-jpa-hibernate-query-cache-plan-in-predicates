package com.asimio.demo.dao.filter;

import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.ParameterExpression;

import org.springframework.data.jpa.domain.Specification;

import com.google.common.collect.Maps;

import lombok.Getter;

@Getter
public abstract class AbstractNamedQueryParametersSpecification<E> implements NamedQueryParametersSpecification<E> {

    protected final Map<String, Object> namedQueryParameters = Maps.newHashMap();
    protected Specification<E> specification;

    protected <T> ParameterExpression<T> createQueryParameter(
            Class<T> namedParamClazz,
            String namedParamName,
            T namedParamValue,
            CriteriaBuilder builder) {

        ParameterExpression<T> result = builder.parameter(namedParamClazz, namedParamName);
        this.namedQueryParameters.put(namedParamName, namedParamValue);
        return result;
    }
}
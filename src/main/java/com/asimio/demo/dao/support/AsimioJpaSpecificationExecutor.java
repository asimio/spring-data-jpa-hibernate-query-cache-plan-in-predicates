package com.asimio.demo.dao.support;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.lang.Nullable;

import com.asimio.demo.dao.filter.NamedQueryParametersSpecification;

@NoRepositoryBean
public interface AsimioJpaSpecificationExecutor<E> extends JpaSpecificationExecutor<E> {

    List<E> findAll(@Nullable NamedQueryParametersSpecification<E> specification);
}
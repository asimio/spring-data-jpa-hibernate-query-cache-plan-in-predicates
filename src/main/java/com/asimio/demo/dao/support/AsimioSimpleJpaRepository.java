package com.asimio.demo.dao.support;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.lang.Nullable;

import com.asimio.demo.dao.filter.NamedQueryParametersSpecification;

public class AsimioSimpleJpaRepository<E, ID extends Serializable> extends SimpleJpaRepository<E, ID>
        implements AsimioJpaSpecificationExecutor<E> {

    public AsimioSimpleJpaRepository(Class<E> domainClass, EntityManager entityManager) {
        super(domainClass, entityManager);
    }

    public AsimioSimpleJpaRepository(JpaEntityInformation<E, ?> entityInformation, EntityManager entityManager) {
        super(entityInformation, entityManager);
    }

    @Override
    public List<E> findAll(@Nullable NamedQueryParametersSpecification<E> specification) {
        TypedQuery<E> query = this.getQuery(specification.getSpecification(), Pageable.unpaged());
        if (specification.getNamedQueryParameters() != null) {
            specification.getNamedQueryParameters().forEach((k, v) -> query.setParameter(k, v));
        }
        return query.getResultList();
    }
}
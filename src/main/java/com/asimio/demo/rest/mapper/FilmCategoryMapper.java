package com.asimio.demo.rest.mapper;

import org.mapstruct.Mapper;

import com.asimio.demo.domain.FilmCategory;

@Mapper
public abstract class FilmCategoryMapper {

    public String map(FilmCategory filmCategory) {
        return filmCategory.getCategory().getName();
    }
}
package com.asimio.demo.rest.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.asimio.demo.domain.Film;
import com.asimio.demo.rest.model.FilmResource;

@Mapper(uses = FilmCategoryMapper.class)
public interface FilmResourceMapper extends ResourceMapper<Film, FilmResource> {

    FilmResourceMapper INSTANCE = Mappers.getMapper(FilmResourceMapper.class);

    @Mappings({
        @Mapping(expression = "java(film.getLanguage().getName().trim())", target = "lang"),
        @Mapping(source = "filmCategories", target = "categories"),
    })
    FilmResource map(Film film);
}
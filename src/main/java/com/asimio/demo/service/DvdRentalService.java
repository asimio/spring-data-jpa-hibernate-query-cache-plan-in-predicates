package com.asimio.demo.service;

import java.util.List;

import com.asimio.demo.domain.Film;

public interface DvdRentalService {

    List<Film> retrieveFilms(FilmSearchCriteria searchCriteria);
}
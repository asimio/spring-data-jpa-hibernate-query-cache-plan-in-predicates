package com.asimio.demo.service.impl;

import java.util.List;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.asimio.demo.dao.FilmDao;
import com.asimio.demo.dao.filter.film.FilmSpecificationsNamedParameters;
import com.asimio.demo.dao.filter.film.FilmSpecificationsNoNamedParameters;
import com.asimio.demo.domain.Film;
import com.asimio.demo.service.DvdRentalService;
import com.asimio.demo.service.FilmSearchCriteria;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
@Transactional(readOnly = true)
public class DefaultDvdRentalService implements DvdRentalService {

    private final FilmDao filmDao;

    @Override
    public List<Film> retrieveFilms(FilmSearchCriteria searchCriteria) {
        return searchCriteria.getWithNamedQueryParameters()
                .map(isNamedParams -> 
                    (Boolean.TRUE.equals(isNamedParams))
                        ? this.retrieveFilmsUsingNamedParameters(searchCriteria)
                        : this.retrieveFilmsWithoutUsingNamedParameters(searchCriteria))
                .orElseGet(() -> this.retrieveFilmsWithoutUsingNamedParameters(searchCriteria));
    }

    /**
     * Retrieve films using {@link Specification} and named query parameters in IN clauses.
     *
     * @param searchCriteria the search criteria
     * @return the list of films
     */
    private List<Film> retrieveFilmsUsingNamedParameters(FilmSearchCriteria searchCriteria) {
        FilmSpecificationsNamedParameters filmSpecifications = FilmSpecificationsNamedParameters.createFilmSpecifications(searchCriteria);
        return this.filmDao.findAll(filmSpecifications);
    }

    /**
     * Retrieve films using {@link Specification} without using named query parameters in IN clauses.
     *
     * @param searchCriteria the search criteria
     * @return the list of films
     */
    private List<Film> retrieveFilmsWithoutUsingNamedParameters(FilmSearchCriteria searchCriteria) {
        Specification<Film> filmSpecifications = FilmSpecificationsNoNamedParameters.createFilmSpecifications(searchCriteria);
        return this.filmDao.findAll(filmSpecifications);
    }
}
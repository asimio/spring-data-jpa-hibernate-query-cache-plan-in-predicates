# README #

Accompanying source code for blog entry at https://tech.asimio.net/2021/04/15/Padding-IN-predicates-using-Spring-Data-JPA-Specification.html


### Requirements ###

* Java 8+
* Maven 3.2.x+
* Docker to start a [asimio/db_dvdrental](https://hub.docker.com/r/asimio/db_dvdrental/) container with the db_dvdrental Postgres database.

```
docker run -d -p 5432:5432 -e DB_NAME=db_dvdrental -e DB_USER=user_dvdrental -e DB_PASSWD=changeit asimio/db_dvdrental:latest
```

### Building the artifact ###

```
mvn clean package
```

### Running the application from command line ###

```
mvn spring-boot:run
```

### Available URLs

```
curl "http://localhost:8080/api/films?minRentalRate=0.5&maxRentalRate=4.99"
curl "http://localhost:8080/api/films?category=Action&category=Comedy&category=Horror&minRentalRate=0.99&maxRentalRate=4.99&releaseYear=2005"
```
should result in successful responses.

### Who do I talk to? ###

* orlando.otero at asimio dot net
* https://www.linkedin.com/in/ootero